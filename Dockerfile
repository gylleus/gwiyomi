FROM ubuntu:16.04

RUN apt-get update && apt-get install -y \
   tree \
   software-properties-common \
   git

RUN add-apt-repository ppa:gophers/archive && \
	apt update && apt-get install -y \
	golang-1.9-go

ENV PATH="/usr/lib/go-1.9/bin/:${PATH}"

RUN mkdir -p /root/go/src/gwiyomi

RUN go get -u github.com/gorilla/mux github.com/go-sql-driver/mysql
RUN go get github.com/stripe/stripe-go
RUN go get gopkg.in/gomail.v2

WORKDIR /root/go/src/gwiyomi

EXPOSE 80
