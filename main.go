package main

import (
	"net/http"
	"database/sql"
	"path/filepath"
	"html/template"
	"os"
	"log"


	_ "github.com/go-sql-driver/mysql"

	"gwiyomi/api"
)

type APIConfig struct {
	
}

// hello world, the web server
func HelloServer(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("hej"))
}

func main() {
	port := ":80"
	println("Listening on port " + port)

	db, err := sql.Open("mysql", "root:dank@tcp(mysql-database:3306)/gwiyomi")
	if err != nil {
		panic(err)
	}
 	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.HandleFunc("/", serveTemplate)
	http.Handle("/api/getProducts", &api.GetProductsHandler{Database: db})
	http.Handle("/api/getCartProducts", &api.GetCartProductsHandler{Database: db})
	http.Handle("/api/addToCart", &api.AddToCartHandler{Database: db})
	http.Handle("/api/clearCart", &api.ClearCartHandler{Database: db})
	http.Handle("/api/updateCartProduct", &api.UpdateCartProductHandler{Database: db})
	http.Handle("/api/charge", &api.ChargeHandler{Database: db})

	log.Println("Listening...")
	http.ListenAndServe(port, nil)
}

func serveTemplate(w http.ResponseWriter, r *http.Request) {
    lp := filepath.Join("static/templates", "layout.html")
	fp := filepath.Join("static/templates", filepath.Clean(r.URL.Path))

	// Return a 404 if the template doesn't exist
	info, err := os.Stat(fp)
	if err != nil {
		if os.IsNotExist(err) {
			http.NotFound(w, r)
			return
		}
	}

	// Return a 404 if the request is for a directory
	if info.IsDir() {
		http.NotFound(w, r)
		return
	}

	templates := template.New("test")
	templates.Delims("<<", ">>")
	tmpl, err := templates.ParseFiles(lp, fp)
	if err != nil {
		// Log the detailed error
		log.Println(err.Error())
		// Return a generic "Internal Server Error" message
		http.Error(w, http.StatusText(500), 500)
		return
	}
	if err := tmpl.ExecuteTemplate(w, "layout", nil); err != nil {
	log.Println(err.Error())
	http.Error(w, http.StatusText(500), 500)
	}
}