var app = angular.module('gwiyomi', []);
app.factory('MessageService', function() {
    errMsg = "";
    showError = "";
    showSuccess = "";
    successMessage = "";
});

app.controller('parentCtrl', function($scope, $http) {
    $scope.parentObj = {};
    $scope.parentObj.cartCount = 0;
});

// Controller for fetching all products
app.controller('getProductsCtrl', function($scope, $http) {
    $scope.category = window.location.search.substr(1).split("=")
    $http({
        method : "POST",
        url    : "/api/getProducts",
        data   : { category: $scope.category[1] }
    }).then(function mySuccess(response) {
        $scope.response = response.data;
        $scope.products = response.data.productList;
    }, function myError(response) {
        $scope.myWelcome = response.statusText;
    });
});

// Controller for fetching one product with specific ID
app.controller('productCtrl', function($scope, $http,$rootScope) {
    $scope.id = window.location.search.substr(1).split("=")
    $http({
        method : "POST",
        url    : "/api/getProducts",
        data   : { id: $scope.id[1] }
    }).then(function mySuccess(response) {
        console.log(response.data);
        $scope.response = response.data;
        $scope.product = response.data.productList[0];
    }, function myError(response) {
        $scope.myWelcome = response.statusText;
    });
});

app.controller('addProductsCtrl', function($scope, $http, $timeout, $rootScope) {
    $scope.quantity = 1;
    $scope.addOneProduct = function() {
        $scope.addToCart($scope.p.id, 1);
    }   
    $scope.addScopeProduct = function() {
        $scope.addToCart($scope.product.id, $scope.quantity);
    }
    $scope.addToCart = function(prodID, quant) {
        if (!quant || !prodID) {
            return;
        }
        $http({
            method : "POST",
            url    : "/api/addToCart",
            data   : { id: prodID, quantity: quant }
        }).then(function mySuccess(response) {
            console.log(response)
            $scope.parentObj.cartCount+=quant;
            showSuccessBox($rootScope, $timeout, "Tillagt i varukorgen!");
        }, function myError(response) {
            console.log(response)
            if (response.data == "NEP") {
                if (quant > 1) {
                   error = "Det finns inte så många av denna produkt kvar just nu! Vi beklagar."                
                } else {
                   error = "Det finns inga fler av denna produkt kvar just nu! Vi beklagar."                    
                }
            } else {
                error = "Ett fel uppstod. Vänligen försök igen senare."
            }
            showErrorBox($rootScope, $timeout, error)
        });
    }
});

function showSuccessBox($scope, $timeout, successMessage) {
    console.log("we did it");
    $scope.successMsg = successMessage;
    $scope.showSuccess = true;
    $timeout(function() {
        $scope.showSuccess = false;
    }, 3000);
}

function showErrorBox($scope, $timeout, errorMessage) {
   $scope.errMsg = errorMessage;
    $scope.showError = true;
    $timeout(function() {
        $scope.showError = false;
    }, 3000);
}

app.controller('cartProductsCtrl', function($scope, $http) {
    $scope.shipping = 49;
    $scope.updateCart = function() {
        $http({
            method : "GET",
            url    : "/api/getCartProducts",
        }).then(function mySuccess(response) {
            $scope.resetCartProducts();
            $scope.cartProducts = response.data.productList;            
            $scope.cartProducts.forEach( function (product) {
                $scope.parentObj.cartCount+=product.quantity;
                product.subTotal = product.quantity * product.price;
                $scope.totalSum += product.subTotal;
            });
        }, function myError(response) {
            console.log(response)
        });
    }
    $scope.clearCart = function() {
        clearCart($scope, $http);
    }
    $scope.resetCartProducts = function() {
        $scope.parentObj.cartCount = 0;
        $scope.totalSum = 0;
        $scope.cartProducts = [];
    }
    $scope.updateCart();
});

app.controller('updateProductCtrl', function($scope, $http) {
    $scope.updateCartProduct = function() {
        $http({
            method : "PUT",
            url    : "/api/updateCartProduct",
            data   : { id: $scope.p.id, quantity: $scope.p.quantity }
        }).then(function mySuccess(response) {
            console.log(response)
            $scope.updateCart();
            }, function myError(response) {
            console.log(response)
        });
    }
});

app.controller('deleteProductCtrl', function($scope, $http) {
    $scope.deleteCartProduct = function() {
    $http({
        method : "PUT",
        url    : "/api/updateCartProduct",
        data   : { id: $scope.p.id, quantity: 0 }
    }).then(function mySuccess(response) {
        console.log(response)
        $scope.updateCart();
        }, function myError(response) {
        console.log(response)
    });

    }
});

app.controller('checkoutCtrl', function($scope, $http, $window) {
    var stripe = Stripe('pk_test_FozeSKKwaN3RKmuyfp7hUa4I');
    var elements = stripe.elements();
    var style = {
    base: {
        fontSize: '16px',
        color: "#32325d",
    }
    };

    // Create an instance of the card Element
    var card = elements.create('card', {style: style});
    card.addEventListener('change', function(event) {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });
    // Add an instance of the card Element into the `card-element` <div>
    card.mount('#card-element');

    $scope.charge = function() {
        event.preventDefault();
        card.address_line1 = $scope.streetAdress
        card.city = $scope.city
        card.address_zip = $scope.zipCode
        stripe.createToken(card).then(function(result) {
        if (result.error) {
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
        } else {
            $http({
                method : "PUT",
                url    : "/api/charge",
                data   : { 
                    paymentMethod: $scope.paymentType, 
                    address:       $scope.streetAdress,
                    city:          $scope.city,
                    zipCode:       $scope.zipCode,
                    firstName:     $scope.firstName,
                    lastName:      $scope.lastName,
                    email:         $scope.email,
                    stripeToken:   result.token.id
                }
            }).then(function mySuccess(response) {
                console.log(response)
                clearCart($scope, $http)
                $window.location.href = '/index.html';
                }, function myError(response) {
                console.log(response)
                window.alert(response.data)
            });
        }
        });        
    }
});

function clearCart($scope, $http) {
    $http({
            method : "GET",
            url    : "/api/clearCart",
        }).then(function mySuccess(response) {
            $scope.resetCartProducts();
        }, function myError(response) {
            console.log(response)
        });
}