package api

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"database/sql"
	"time"
	"strconv"

	"github.com/stripe/stripe-go"
 	"github.com/stripe/stripe-go/currency"
	"github.com/stripe/stripe-go/charge"
//	"gopkg.in/gomail.v2"

	"gwiyomi/utils"
	"gwiyomi/api/objects"
)

type ChargeRequest struct {
	PaymentMethod string `json:"paymentMethod"`
	City          string `json:"city"`
	ZipCode       string `json:"zipCode"`
	Address       string `json:"address"`
	FirstName     string `json:"firstName"`
	LastName      string `json:"lastName"`
	Email         string `json:"email"`
	StripeToken   string `json:"stripeToken"`
	Currency      string `json"currency"`
}

type ChargeHandler struct {
	Database *sql.DB
}

func (handler ChargeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	chargeReq := ChargeRequest{}
	json.Unmarshal(body, &chargeReq)

	println(chargeReq.StripeToken)

	userID, err := r.Cookie("userID")
	if err != nil {
		println(err.Error())
	} else {
		db := handler.Database
		products, err := GetUserProducts(userID.Value, db)
		if err != nil {
			utils.HandleError(w, "Error reading cart products from database", http.StatusInternalServerError, err)
			return
		}

		totalPrice := 0
		for _, p := range products.ProductList {
			totalPrice += p.Price * p.Quantity
		} 
		if chargeReq.PaymentMethod == "creditCard" {
			errMsg, err := stripeCharge(totalPrice, userID.Value, products, chargeReq)
			if err != nil {
				utils.HandleError(w, errMsg, http.StatusInternalServerError, err)
				return
			} else {
				println("Stripe payment successful for user " + userID.Value)
			}	
		}
		//Put into database as successful order
		timeOfOrder := utils.TimeString(time.Now())
		res, err := db.Exec("INSERT INTO Orders VALUES(0,?,?,?,?,?,?,?,?,?,?)", userID.Value, timeOfOrder, 
			chargeReq.Email, chargeReq.City, chargeReq.Address,	chargeReq.ZipCode, chargeReq.FirstName, 
			chargeReq.LastName, chargeReq.PaymentMethod, totalPrice)

		if err != nil {
			println("\n!! ERROR STORING PAYED ORDER IN DATABASE !!\n" + err.Error())
			println("ORDER AT TIME + " + string(timeOfOrder) + ":\n" + string(body))
			return
		} else {
			orderID, err := res.LastInsertId()
			if err != nil {
				println("Error getting orderID!\n" + err.Error())
			}
			for _, p := range products.ProductList {
				_, err = db.Exec("INSERT INTO OrderedProducts VALUES(0,?,?,?)", p.ID, p.Quantity, orderID)
				if err != nil {
					println("!! ERROR STORING PRODUCT " + strconv.Itoa(p.ID) + " FOR USER " + userID.Value + "\n" + err.Error())
				} 
			}
		}

		for _, p := range products.ProductList {
			_, err = db.Exec("UPDATE Products SET Quantity = Quantity - ? WHERE id = ?", p.Quantity, p.ID)
			if err != nil {
				println("!! ERROR SUBTRACTING QUANTITY FOR PRODUCT " + strconv.Itoa(p.ID) + "\n" + err.Error())
			} 
		}
		// Send order email
	}

	w.Write([]byte("OK"))
}

func stripeCharge(amount int, userID string, products api.Products, chargeReq ChargeRequest) (string, error) {
	stripe.Key = "sk_test_ySMava8iJYeOEwYo9n4NxIoo"
	crDesc, err := json.Marshal(chargeReq)
	
	if err != nil {
		return "Internal server error.", err
	}

	chargeParams := &stripe.ChargeParams{
	  Amount: uint64(amount*100),
	  Currency: currency.SEK,
	  Desc: "Charge for " + chargeReq.FirstName + " " + chargeReq.LastName + ":\n" + string(crDesc),
	}
	chargeParams.SetSource(chargeReq.StripeToken) // obtained with Stripe.js
	_, err = charge.New(chargeParams)

	if err != nil {
	 	errorMessage := ""
		if  stripeErr, ok := err.(*stripe.Error); ok {
		// The Code field will contain a basic identifier for the failure.
			switch stripeErr.Code {
			case stripe.IncorrectNum:
				errorMessage = "Incorrect card number."
			case stripe.InvalidNum:
				errorMessage = "Invalid card number."
			case stripe.InvalidExpM:
				errorMessage = "Invalid expire date month."
			case stripe.InvalidExpY:
				errorMessage = "Invalid expire date year."
			case stripe.InvalidCvc:
				errorMessage = "Invalid CVC number."
			case stripe.ExpiredCard:
				errorMessage = "Card has expired."
			case stripe.IncorrectCvc:
				errorMessage = "Incorrect CVC number."
			case stripe.CardDeclined:
				errorMessage = "Card declined."
			case stripe.Missing:
				errorMessage = "Missing card information."
			case stripe.ProcessingErr:
				errorMessage = "Error processing card."
			default:
				errorMessage += "Unknown error."
			}
		} else {
			errorMessage += "Unknown error occurred."
		}
		errorMessage += " Please try again."
		return errorMessage, err		
	}
	return "", nil
}

/*
func sendEmails() {

}

func sendOrderConfirmation() {
	message := gomail.NewMessage()

	message.SetHeaders(map[string][]string {
		"From": {m.FormatAddress("info@gwiyomi.se", "Gwiyomi")},
		"To": {m.FormatAddress(to, toName)},
		"Subject": {"Bekräftelse för din order - #" + orderNr},
		})

	message.SetAddressHeader()
}

func sendOrderAlter() {

}*/