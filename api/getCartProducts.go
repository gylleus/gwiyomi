package api

import (
	"net/http"
	"encoding/json"
	"database/sql"

	"gwiyomi/api/objects"
	"gwiyomi/utils"
)

type GetCartProductsHandler struct {
	Database *sql.DB
}

func (handler GetCartProductsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var resp []byte
	cookie, err := r.Cookie("userID")
	if err != nil {
		println(err.Error())
	} else {
		db := handler.Database
		products, err := GetUserProducts(cookie.Value, db)
		if err != nil {
			utils.HandleError(w, "Error reading cart products from database", http.StatusInternalServerError, err)
			return
		}
		resp, err = json.Marshal(products)
		if err != nil {
			utils.HandleError(w, "Error marshalling JSON", http.StatusInternalServerError, err)
			return
		}
	}
	w.Write([]byte(resp))
}

func GetUserProducts(userID string, db *sql.DB) (api.Products, error) {
	products := api.Products{ProductList: make([]api.Product,0)}
	
	println("Retrieving cart products for user " + userID)
	rows, err := db.Query("SELECT productID, quantity FROM CartProducts WHERE customerID=?", userID)
	if err != nil {
		return api.Products{}, err
	}
	defer rows.Close()
	for rows.Next() {
		var _productID int
		var _quantity int
		var _name string
		var _category string
		var _price int
		var _imageFolder string

		err = rows.Scan(&_productID, &_quantity)
		if err != nil {
			println(err.Error())
			continue
		}
		// Read product information from Product table
		err = db.QueryRow("SELECT name, category, price, imageName FROM Products WHERE id=?", _productID).Scan(&_name, &_category, &_price, &_imageFolder)
		if err != nil {
			println(err.Error())
			continue
		}
		_, thumbnail, err := utils.GetProductImages(_imageFolder)
		if err != nil {
			println(err.Error())
			continue
		}
		products.ProductList = append(products.ProductList, api.Product{ID: _productID, Name: _name, Category: _category, Price: _price, Thumbnail: thumbnail, Quantity: _quantity})
	}
	err = rows.Err()
	if err != nil {
		return api.Products{}, err
	}
	return products, nil
}