package api

import (
	"net/http"
	"database/sql"

	"gwiyomi/utils"
)

type ClearCartHandler struct {
	Database *sql.DB
}

func (handler ClearCartHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	db := handler.Database

	cookie, err := r.Cookie("userID")

	if err != nil {
		println(err.Error())
	} else {
		println("Clearing cart for user " + cookie.Value)
		_, err = db.Exec("DELETE FROM CartProducts WHERE customerID=?", cookie.Value)
		if err != nil {
			utils.HandleError(w, "Error clearing cart", http.StatusInternalServerError, err)
		}
	}
	w.Write([]byte("OK"))
}