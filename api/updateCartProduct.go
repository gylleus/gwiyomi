package api

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"database/sql"
	"strconv"

	"gwiyomi/utils"
)

type apiRequest struct {
	ID       int `json:"id"`
	Quantity int `json:"quantity"`
}

type UpdateCartProductHandler struct {
	Database *sql.DB
}

func (handler UpdateCartProductHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.HandleError(w, "Error reading updateCartProduct request", http.StatusBadRequest, err)
		return
	}
	req := apiRequest{}
	json.Unmarshal(body, &req)
	db := handler.Database
	println(string(body))
	cookie, err := r.Cookie("userID")

	switch {
	case err != nil:	
		println(err.Error())
	case req.Quantity <= 0:
		println("Deleting product " + strconv.Itoa(req.ID) + " for user " + cookie.Value)
		_, err = db.Exec("DELETE FROM CartProducts WHERE customerID=? AND productID=?", cookie.Value, req.ID)
		if err != nil {
			utils.HandleError(w, "Error removing cart item", http.StatusInternalServerError, err)
		}
	default:
		println("Updating product " + strconv.Itoa(req.ID) + " with quantity " + strconv.Itoa(req.Quantity) + " for user " + cookie.Value)
		_, err = db.Exec("UPDATE CartProducts SET quantity=? WHERE customerID=? AND productID=?", req.Quantity, cookie.Value, req.ID)
		if err != nil {
			utils.HandleError(w, "Error updating cart item", http.StatusInternalServerError, err)
		}
	}
	w.Write([]byte("OK"))
}