package api

import (
	"database/sql"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"

	"gwiyomi/utils"
)

type AddToCartRequest struct {
	ID       int `json:"id"`
	Quantity int `json:"quantity"`
}

type AddToCartHandler struct {
	Database *sql.DB
}

func (handler AddToCartHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.HandleError(w, "Error reading addToCart request", http.StatusBadRequest, err)
		return
	}
	addToCartReq := AddToCartRequest{}
	json.Unmarshal(body, &addToCartReq)
	db := handler.Database

	cookie, err := r.Cookie("userID")

	if err != nil {
		newCookie, err := utils.CreateCookie(db)
		if err != nil {
			utils.HandleError(w, "Unable to create cookie", http.StatusInternalServerError, err)
			return
		}
		http.SetCookie(w, &newCookie)
		cookie = &newCookie
	}

	// Check if already in database, in that case update instead of remove
	rows, err := db.Query("SELECT quantity FROM CartProducts WHERE customerID=? AND productID=?", cookie.Value, addToCartReq.ID)
	if err != nil {
		utils.HandleError(w, "Error fetching product row from CartProducts", http.StatusInternalServerError, err)
		return
	}
	if rows.Next() {
		var currentQuantity int
		rows.Scan(&currentQuantity)

		var totalQuantity int
		pr, err := db.Query("SELECT quantity FROM Products WHERE id=?", addToCartReq.ID)
		if !pr.Next() {
			utils.HandleError(w, "Unknown product", http.StatusBadRequest, err)
			return
		}
		pr.Scan(&totalQuantity)

		if totalQuantity < currentQuantity+addToCartReq.Quantity {
			utils.HandleError(w, "NEP", http.StatusBadRequest, errors.New("Not enough products left. Prod ID: "+strconv.Itoa(addToCartReq.ID)))
			return
		}

		println("Updating entry for user " + cookie.Value + " for product " + strconv.Itoa(addToCartReq.ID) + " with quantity " + strconv.Itoa(currentQuantity+addToCartReq.Quantity))
		_, err = db.Exec("UPDATE CartProducts SET quantity=? WHERE customerID=? AND productID=?", currentQuantity+addToCartReq.Quantity, cookie.Value, addToCartReq.ID)
	} else {
		var totalQuantity int
		pr, err := db.Query("SELECT quantity FROM Products WHERE id=?", addToCartReq.ID)
		if !pr.Next() {
			utils.HandleError(w, "Unknown product", http.StatusBadRequest, err)
			return
		}
		pr.Scan(&totalQuantity)

		if totalQuantity < addToCartReq.Quantity {
			utils.HandleError(w, "NEP", http.StatusBadRequest, errors.New("Not enough products left. Prod ID: "+strconv.Itoa(addToCartReq.ID)))
			return
		}
		_, err = db.Exec("INSERT INTO CartProducts VALUES(0,?,?,?)", cookie.Value, addToCartReq.ID, addToCartReq.Quantity)
	}

	if err != nil {
		utils.HandleError(w, "Error updating quantity for item", http.StatusInternalServerError, err)
		return
	}

	w.Write([]byte("OK"))
}
