package api

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"database/sql"
	"gwiyomi/api/objects"

	"gwiyomi/utils"
)

type ProductsRequest struct {
	Category string `json:"category"`
	ID       string `json:"id"`
}

type GetProductsHandler struct {
	Database *sql.DB
}

func (handler GetProductsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.HandleError(w, "Error reading getProducts request", http.StatusBadRequest, err)
		return
	}
	prodReq := ProductsRequest{}
	json.Unmarshal(body, &prodReq)
	db := handler.Database

	products := api.Products{ProductList: make([]api.Product,0)}

	println("Getting products: " + string(body))

	var rows *sql.Rows
	switch {
	case prodReq.Category != "":
		rows, err = db.Query("SELECT * FROM Products WHERE Category=?",prodReq.Category)
	case prodReq.ID != "":
		rows, err = db.Query("SELECT * FROM Products WHERE id=?",prodReq.ID)
	default:
		rows, err = db.Query("SELECT * FROM Products")		
	}

	if err != nil {
		utils.HandleError(w, "Error reading products from database", http.StatusInternalServerError, err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var _id int
		var _name string
		var _category string
		var _price int
		var _imageFolder string
		var _quantity int
		var _description string

		err = rows.Scan(&_id, &_name, &_category, &_price, &_imageFolder, &_quantity, &_description)
		if err != nil {
			println(err.Error())
			return
		}
		images, thumbnail, err := utils.GetProductImages(_imageFolder)
		if err != nil {
			utils.HandleError(w, "Error getting product images", http.StatusInternalServerError, err)
			return
		}
		products.ProductList = append(products.ProductList, api.Product{
			ID: _id, Name: _name, Category: _category, Price: _price, 
			Images: images, Thumbnail: thumbnail, Quantity: _quantity, Description: _description})
	}
	err = rows.Err()
	if err != nil {
		utils.HandleError(w, "Error reading rows", http.StatusInternalServerError, err)
		return
	}
	resp, err := json.Marshal(products)
	if err != nil {
		utils.HandleError(w, "Error marshalling JSON", http.StatusInternalServerError, err)
		return
	}

	w.Write([]byte(resp))
}

