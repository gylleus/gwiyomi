package api

type Product struct {
	ID          int      `json:"id"'`
	Name        string   `json:"name"`
	Category    string   `json:"category"`
	Price       int      `json:"price"`
	Images      []string `json:"images"`
	Thumbnail   string   `json:"thumbnail"`
	Quantity    int      `json:"quantity"`
	Description string   `json:"description"`
}

type Products struct {
	ProductList []Product `json:"productList"`
}