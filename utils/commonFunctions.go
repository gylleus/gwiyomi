package utils

import (
	"math/rand"
	"time"
	"log"
	"net/http"
	"database/sql"
	"os"
	"path/filepath"

)

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789")

func CreateCookie(db *sql.DB) (http.Cookie, error) {
	cookieValue := GenerateCookieValue(30)
	
	var exists bool
	err := db.QueryRow("SELECT EXISTS(SELECT * FROM Customers WHERE id=?)", cookieValue).Scan(&exists)
	if err != nil {
		return http.Cookie{}, err
	}
	// Check that no cookie has this value already
	if exists {
		println("Cookie " + cookieValue + " already exists! Creating new one...")
		return CreateCookie(db)
	} else {
		expire := time.Now().Add(5 * 24 * time.Hour)
		newCookie := http.Cookie {Name: "userID", Value: cookieValue, Expires: expire }
		createdFormat := TimeString(time.Now())
		expireFormat := TimeString(expire)
		// Insert new customer into database
		println("Inserting new customer cookie " + cookieValue + " into Customers")
		_, err := db.Exec("INSERT INTO Customers Values(?,?,?)", cookieValue, createdFormat, expireFormat)
		if err != nil {
			return http.Cookie{}, err
		}	
		return newCookie, nil		
	}
}

/*

TODO: Update cookie for customer so it doesn't expire if they keep using it

*/

func TimeString(t time.Time) string {
    return t.Format("2006-01-02 15:04:05.999999")
}

func GenerateCookieValue(length int) string {
	rand.Seed(time.Now().UnixNano())
    value := make([]rune, length)
    for i := range value {
        value[i] = letters[rand.Intn(len(letters))]
    }
    return string(value)
}

func HandleError(w http.ResponseWriter, message string, statusCode int, err error) {
	w.WriteHeader(statusCode)
	w.Write([]byte(message))
	fullError := message + ":\n" + err.Error()
	log.Println(fullError)
}


// Returns all images, thumbnail, error
func GetProductImages(folderName string) ([]string, string, error) {
	staticPath := "static/images/"
	file, err := os.Open("static/images/" + folderName)
	if err != nil {
		return nil, "", err
	}
	images, err := file.Readdir(0)
	if err != nil {
		return nil, "", err
	}
	imageNames := make([]string,0)
	thumbnail := ""
	for i := range images {
		imgName := images[i].Name()
		fullPath := filepath.Join(staticPath, folderName, imgName)
		if imgName == "thumbnail" + filepath.Ext(imgName) {
			thumbnail = fullPath
		} else {
			imageNames = append(imageNames, fullPath)
		}
	}
	return imageNames, thumbnail, nil
}